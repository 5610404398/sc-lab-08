package Controller;
import Interface.Measurable;
import Model.BankAccount;
import Model.Company;
import Model.Country;
import Model.Data;
import Model.Person;
import Model.Product;
import Model.TaxCalculater;
import Interface.Taxable;

import java.util.ArrayList;
public class ControlTest {

	public static void main(String[] args) {
		ControlTest test = new ControlTest();
		test.Testcase1();
		test.Testcase2();
		test.Testcase3();
	}
	
	public void Testcase1(){
		System.out.println("------------------------------");
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("Baitoey",158.0);
		persons[1] = new Person("OA",165.0);
		persons[2] = new Person("Taa",170.0);
		System.out.println("Person avg tall: "+Data.average(persons));
	}
	
	public void Testcase2(){
		System.out.println("------------------------------");
		Measurable[] bankvscountry = new Measurable[3];
		Person p1 = new Person("Baitoey",156.0);
		Person p2 = new Person("OA",175.0);
		BankAccount b1 = new BankAccount("Baitoey",20000);
		BankAccount b2 = new BankAccount("Baitong",15000);
		Country c1 = new Country("Thailand",543456324);
		Country c2 = new Country("Laos",3233222);
		
		bankvscountry[0] = Data.measurable_min(b1, b2);
		System.out.println("Bank Min: "+bankvscountry[0].getMeasurable());
		bankvscountry[1] = Data.measurable_min(c1, c2);
		System.out.println("Country Min: "+bankvscountry[1].getMeasurable());
		bankvscountry[2] = Data.measurable_min(p1, p2);
		System.out.println("Pesron Min: "+bankvscountry[2].getMeasurable());
		
	}
	public void Testcase3(){
		System.out.println("------------------------------");
		ArrayList<Taxable> persons = new ArrayList<Taxable>();
		ArrayList<Taxable> companies = new ArrayList<Taxable>();
		ArrayList<Taxable> products = new ArrayList<Taxable>();
		ArrayList<Taxable> all = new ArrayList<Taxable>();
		
		Person p1 = new Person("Baitoey",7300000);
		Person p2 = new Person("OA",450000);
		Person p3 = new Person("Taa",350000);
		persons.add(p1);
		persons.add(p2);
		persons.add(p3);
		
		Company c1 = new Company("A",1500000,400000);
		Company c2 = new Company("B",4508545,1322112);
		Company c3 = new Company("C",10000000,1234321);
		companies.add(c1);
		companies.add(c2);
		companies.add(c3);
		
		Product pd1 = new Product("Champoo",129);
		Product pd2 = new Product("Food",50);
		Product pd3 = new Product("BB-Cream",378);
		products.add(pd1);
		products.add(pd2);
		products.add(pd3);
		
		all.add(pd1);
		all.add(p1);
		all.add(c2);
		
		System.out.println("Persons sum tax: "+TaxCalculater.sum(persons));
		System.out.println("Companies sum tax: "+TaxCalculater.sum(companies));
		System.out.println("Produxt sum tax: "+TaxCalculater.sum(products));
		System.out.println("All sum tax: "+TaxCalculater.sum(all));
	}
	
	
	
}
