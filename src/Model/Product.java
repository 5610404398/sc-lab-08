package Model;
import Interface.Taxable;

public class Product  implements Taxable{
	String name;
	double price; 

	public Product(String name,double price){
		this.name = name;
		this.price = price;
		
	}
	@Override
	public double getTax() {
		return (price*7)/100;
	}
}
