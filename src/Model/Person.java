package Model;
import Interface.Measurable;
import Interface.Taxable;

public class Person implements Measurable,Taxable {
	String name;
	double tall; 
	double income;
	double tax;
	public Person(String name,double tall){
		this.name = name;
		this.tall = tall;
	}
	
	public Person(String name,int income){
		this.name = name;
		this.income = income;
	}
	
	@Override
	public double getMeasurable() {
		return tall;
	}

	@Override
	public double getTax() {
		if (income <= 300000){
			return (income*5)/100;
		}
		else {
			return 15000+(((income-300000)*10)/100);
		}
	}



	
	
}
