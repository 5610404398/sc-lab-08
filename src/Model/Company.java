package Model;
import Interface.Taxable;

public class Company implements Taxable {
	String name;
	double expense; 
	double income;
	public Company(String name,double income,double expense){
		this.name = name;
		this.income = income;
		this.expense = expense;
	}
	@Override
	public double getTax() {
		if (income > expense){
			return ((income-expense)*30)/100;
		}
		else {
			return 0;
		}
		
	}
}
