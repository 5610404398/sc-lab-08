package Model;

import Interface.Measurable;

public class Data {
	public static double average(Measurable[] a){
		double sum = 0;
		for (Measurable m : a ){
			sum += m.getMeasurable(); ;
		}
		if (a.length > 0){
			return sum/a.length;
		}
		return 0;
	}
	
	public static Measurable measurable_min(Measurable m1, Measurable m2){
		if (m1.getMeasurable() < m2.getMeasurable()){
			return m1;
		}
		else {
			return m2;
		}		
	}
}
